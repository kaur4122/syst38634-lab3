package PasswordValidator;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Time.Time;

public class passwordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test1CheckPasswordLength() {
		String password = "N23preet";
		int nav = passwordValidator.checkPasswordLength(password);
		int dav = passwordValidator.checkDigits(password);
	    assertTrue("The password length is correct, Happy", nav>=8 && dav >=2);
	    System.out.println("The password length is correct, Happy");	    	
	}
	
	@Test
	public void test2CheckPasswordLength() {
		String password = "Navpr23e";
			int nav = passwordValidator.checkPasswordLength(password);	
			int dav = passwordValidator.checkDigits(password);
		    assertTrue("The password length is incorrect, Sad", nav>=8 && dav >=2);
		    System.out.println("The password length is incorrect, Sad");			
	}

	@Test
	public void test3CheckPasswordLength() {
		String password = "Nav233pee";
			int nav = passwordValidator.checkPasswordLength(password);
			int dav = passwordValidator.checkDigits(password);
		    assertTrue("The password length is incorrect, Sad", nav>=8 && dav >=2);
		    System.out.println("The password length is incorrect, Sad");	
    }
	
	@Test
	public void test4CheckPasswordLength() {
		String password = "Nav233rrrr";
			int nav = passwordValidator.checkPasswordLength(password);		
			int dav = passwordValidator.checkDigits(password);
		    assertTrue("The password length is incorrect, Sad", nav>=8 && dav >=2);
		    System.out.println("The password length is incorrect, Sad");		
    }	
}
