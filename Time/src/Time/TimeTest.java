package Time;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TimeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void getMilliseconds() {
		int nav = Time.getMilliSeconds("12:05:05:05");		
	    assertTrue("The milliseconds were not calculated propertly", nav==5);
	    System.out.println("The milliseconds were not calculated propertly");
	}

	@Test (expected = NumberFormatException.class)
	public void getMillisecondsException() {
		int nav = Time.getMilliSeconds("12:05:05:0054");
		assertTrue("The milliseconds were not calculated propertly", nav==5);
	}
	
	@Test
	public void getMillisecondsBoundaryIN() {
		int nav = Time.getMilliSeconds("12:05:05:123");
			assertTrue("The milliseconds were not calculated propertly", nav==123);
	}
   	public void getMillisecondsBoundaryOUT() {
		int nav = Time.getMilliSeconds("12:05:05:1000");
			assertTrue("The milliseconds were not calculated propertly", nav==0);
}

}
